package vmorales;


import javax.media.opengl.GL;
import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.glu.GLU;
//Libreria para frame
import javax.swing.JFrame;
//librerias para teclas
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import com.sun.opengl.util.Animator;
import static javax.swing.JFrame.EXIT_ON_CLOSE;


public class Castillo extends JFrame implements KeyListener{

    public Castillo() {
        setSize(800, 700);
        setLocation(10, 40);
        setTitle("Castillo");
        GraphicListener listener = new GraphicListener();
        GLCanvas canvas = new GLCanvas(new GLCapabilities());
        canvas.addGLEventListener(listener);
        getContentPane().add(canvas);        
        Animator animator = new Animator(canvas);
        animator.start();        
        addKeyListener(this);
    }

    public static void main(String args[]) {
        Castillo myframe = new Castillo();
        myframe.setVisible(true);
        myframe.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }
    //Variables para instanciar funciones de opengl
    static GL gl;
    static GLU glu;

    //Variables para las transformaciones
    private static float trasladaX=0;
    private static float trasladaY=0;
    static float mb=0;
    private static float rotarX=0;
    private static float rotarY=0;
    private static float rotarZ=0;

    public class GraphicListener implements GLEventListener {

        public void display(GLAutoDrawable arg0) {
            //Inicializamos las variables
            //INICIALIZA GLU
            glu = new GLU();
            //Inicializa Gl
            gl = arg0.getGL();

            gl.glClear(gl.GL_COLOR_BUFFER_BIT);
            //Establecemos el color de fondo de la ventana de colores RGB
            gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);            
            //Establecemos los parametros de proyeccion
            gl.glMatrixMode(GL.GL_PROJECTION);
//            gl.glOrtho(0.0, 1.0, 0.0, 1.0, -1.0, 1.0);
           
            //Matriz identidad
            gl.glLoadIdentity();

            //Dibuja cielo
            Rectangulo rcielo = new Rectangulo();
            rcielo.CrearRectangulo(1, 0, -1, 0, -1, 1, 1, 1, 0.74902f,0.847059f,0.847059f);
            
            //Dibuja Sol
            gl.glPushMatrix();
            gl.glRotatef(rotarZ,0, 0, 2);
            Sol sol= new Sol();
            sol.DrawSol();
            rotarZ=rotarZ+0.2f;
            gl.glPopMatrix();
     
            //Dibuja Cesped
            Rectangulo rCesped= new Rectangulo();
            rCesped.CrearRectangulo(1, 0, -1, 0, -1, -1, 1, -1,0.419608f, 0.556863f, 0.137255f);
            
            //Dibuja banderas
            gl.glPushMatrix();
            gl.glRotatef(mb,1,0,0);
            gl.glTranslatef(0,trasladaY, -2.2f);
            gl.glRotatef(0,1,0,0);
            gl.glTranslatef(0, -trasladaY, 2.1f);
     
            mb=mb+0.3f;
            Banderas banderas= new Banderas ();
            banderas.DibujaBanderas();
            trasladaY=trasladaY+0.001f;
            gl.glPopMatrix();
            
            //Dibuja Castillo
            gl.glPushMatrix();
            DCastillo castillo = new DCastillo();
            castillo.DibujaCastillo();
            gl.glPopMatrix();
            
            //Dibuja Rey
            gl.glPushMatrix();
            //Aplicar movimiento al Rey
            gl.glTranslatef(trasladaX, 0, 0);
            Rey rey = new Rey ();
            rey.DibujaRey();
            gl.glPopMatrix();
            
            
             gl.glFlush();
            
        }

        
        public void displayChanged(GLAutoDrawable drawable, boolean modeChanged, boolean deviceChanged) {
        }

        //En este metodo se queda el mismo segmento que hemos venido manejando
        public void init(GLAutoDrawable arg0) {
            GL gl = arg0.getGL();
            gl.glEnable(GL.GL_BLEND);
            gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        }

        public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        }
    }

    //Metodo para el teclado
    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_RIGHT){
            trasladaX+=.1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

        if(e.getKeyCode() == KeyEvent.VK_LEFT){
            trasladaX-=.1f;
            System.out.println("Valor en la traslacion de X: " + trasladaX);
        }

       
         if(e.getKeyCode() == KeyEvent.VK_R){
            rotarX+=1f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_T){
            rotarX-=1f;
            System.out.println("Valor en la rotacion en X: " + rotarX);
        }

        if(e.getKeyCode() == KeyEvent.VK_Y){
            rotarY+=1f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

        if(e.getKeyCode() == KeyEvent.VK_U){
            rotarY-=1f;
            System.out.println("Valor en la rotacion en Y: " + rotarY);
        }

         if(e.getKeyCode() == KeyEvent.VK_ESCAPE){
            trasladaX=0;
            rotarX=0;
            rotarY=0;
        }
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }
}

