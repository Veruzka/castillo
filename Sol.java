/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vmorales;

import static vmorales.Castillo.gl;

/**
 *
 * @author jose_
 */
public class Sol {
   
    public void DrawSol (){
        //Dibuja circunferencia
        float radio=0.1f;
        gl.glTranslatef(0f,0.9f,0);
        gl.glBegin(gl.GL_POLYGON);
            for(int i=0; i<100; i++){
               float x = (float) Math.cos(i*2*3.1416/100);
               float y = (float) Math.sin(i*2*3.1416/100);
               gl.glColor3f(1.0f, 0.8f, 0f);
               gl.glVertex2f(radio*x,radio*y); 
            }
        gl.glEnd();
        gl.glFlush();
    }
            
}
