

package vmorales;

/**
 *
 * @author jose_
 */
public class DCastillo {
    public void DibujaCastillo(){
        
        Rectangulo r1=new Rectangulo ();
        r1.CrearRectangulo( -0.2f, -0.5f, 0.4f,-0.5f, 0.4f, 0f, -0.2f, 0f,0.558824f,0.558824f,0.558824f);//primer piso central 
        Rectangulo r2=new Rectangulo ();
        r2.CrearRectangulo(  -0.1f, 0, 0.3f, 0, 0.3f, 0.45f, -0.1f, 0.45f, 0.3f, 0.3f,0.3f);//torre central 
        Rectangulo r3=new Rectangulo ();
        r3.CrearRectangulo( -0.13f, 0.4f, 0.33f, 0.4f, 0.33f, 0.45f, -0.13f, 0.45f, 0.3555f, 0.3555f,0.3555f);//borde de torre central
        Cuadrado c1= new Cuadrado();
        c1.CrearCuadrado(  -0.2f, 0, -0.13f,0, -0.13f, 0.07f, -0.2f, 0.07f,0.558824f, 0.558824f,0.558824f);//cuadrado1 piso central
        Cuadrado c2= new Cuadrado();
        c2.CrearCuadrado( 0.33f, 0, 0.4f,0, 0.4f, 0.07f, 0.33f, 0.07f,0.558824f, 0.558824f,0.558824f);//cuadrado2 piso central
        Cuadrado c3= new Cuadrado();
        c3.CrearCuadrado(-0.10f, 0, -0.02f,0, -0.02f, 0.07f, -0.10f, 0.07f,0.558824f,0.558824f,0.558824f);//cuadrado3 piso central
        Cuadrado c4= new Cuadrado();
        c4.CrearCuadrado( 0.227f, 0, 0.3f,0, 0.3f, 0.07f, 0.227f, 0.07f,0.558824f, 0.558824f,0.558824f);//cuadrad04 piso central
        Cuadrado c5= new Cuadrado();
        c5.CrearCuadrado( 0.01f, 0, 0.09f,0, 0.09f, 0.07f, 0.01f, 0.07f,0.558824f,0.558824f,0.558824f);//cuadrado5 piso central
        Cuadrado c6= new Cuadrado();
        c6.CrearCuadrado( 0.2f, 0, 0.12f,0, 0.12f, 0.07f, 0.2f, 0.07f,0.558824f, 0.558824f,0.558824f);//cuadrado6 piso central
        Rectangulo r4=new Rectangulo ();
        r4.CrearRectangulo( 0.4f, -0.5f, 0.8f,-0.5f, 0.8f, 0.2f, 0.4f, 0.2f,0.3555f, 0.3555f,0.3555f);//torre derecha
        Rectangulo r5=new Rectangulo ();
        r5.CrearRectangulo( -0.2f, -0.5f, -0.6f,-0.5f, -0.6f, 0.2f, -0.2f, 0.2f,0.3555f, 0.3555f,0.3555f);//torre izquierda
        Rectangulo r6=new Rectangulo ();
        r6.CrearRectangulo(-0.1f, -0.5f, 0.3f,-0.5f, 0.3f, -0.1f, -0.1f, -0.1f, 0.36f, 0.25f,0.20f);//Borde puerta 
        Rectangulo r7=new Rectangulo ();
        r7.CrearRectangulo(-0.09f, -0.5f, 0.1f,-0.5f, 0.1f, -0.11f, -0.09f, -0.11f,0.35f,0.16f,0.14f);//puerta izquierda 
        Rectangulo r8=new Rectangulo ();
        r8.CrearRectangulo(0.11f, -0.5f, 0.29f,-0.5f, 0.29f, -0.11f, 0.11f, -0.11f,0.35f,0.16f,0.14f);//puerta derecha 
        Cuadrado c8= new Cuadrado();
        c8.CrearCuadrado(0.4f,0.2f,0.47f,0.2f,0.47f,0.27f,0.4f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado1 torre derecha
        Cuadrado c9= new Cuadrado();
        c9.CrearCuadrado( 0.73f,0.2f,0.8f,0.2f,0.8f,0.27f,0.73f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado2 torre derecha
        Cuadrado c10= new Cuadrado();
        c10.CrearCuadrado( 0.51f,0.2f,0.58f,0.2f,0.58f,0.27f,0.51f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado3 torre derecha
        Cuadrado c11= new Cuadrado();
        c11.CrearCuadrado(  0.62f,0.2f,0.69f,0.2f,0.69f,0.27f,0.62f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado4 torre derecha
        Cuadrado c12= new Cuadrado();
        c12.CrearCuadrado( -0.6f,0.2f,-0.53f,0.2f,-0.53f,0.27f,-0.6f,0.27f,0.3555f,0.3555f,0.3555f);//cuadrado1 torre izquierda
        Cuadrado c13= new Cuadrado();
        c13.CrearCuadrado( -0.2f,0.2f,-0.27f,0.2f,-0.27f,0.27f,-0.2f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado2 torre izquierda
        Cuadrado c14= new Cuadrado();
        c14.CrearCuadrado(-0.31f,0.2f,-0.38f,0.2f,-0.38f,0.27f,-0.31f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado3 torre izquierda
        Cuadrado c15= new Cuadrado();
        c15.CrearCuadrado( -0.42f,0.2f,-0.49f,0.2f,-0.49f,0.27f,-0.42f,0.27f,0.3555f, 0.3555f,0.3555f);//cuadrado4 torre izquierda
        Cuadrado c16= new Cuadrado();
        c16.CrearCuadrado( -0.39f,0.2f,-0.4f,0.2f,-0.4f,0.39f,-0.39f,0.39f,0.35f, 0.16f,0.14f);//palito bandera izquierda 
        Cuadrado c17= new Cuadrado();
        c17.CrearCuadrado( 0.61f,0.2f,0.6f,0.2f,0.6f,0.39f,0.61f,0.39f,0.35f, 0.16f,0.14f);//palito bandera derecha
    }
}
